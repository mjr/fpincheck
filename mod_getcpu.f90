module mod_getcpu

  interface
    function threadGetProcessorId( ) bind( c, name="threadGetProcessorId" )
      use, intrinsic :: iso_c_binding
      integer( kind = c_int )        :: threadGetProcessorId
    end function threadGetProcessorId
  end interface

  interface
    function where_am_i_running( ) bind( c, name="where_am_i_running" )
      use, intrinsic :: iso_c_binding
      integer( kind = c_int )        :: where_am_i_running
    end function where_am_i_running
  end interface

  interface !to function: int gethostname(char *name, size_t namelen);
     integer(c_int) function gethostname(name, namelen) bind(c)
       use, intrinsic  :: iso_c_binding, only: c_char, c_int, c_size_t
       integer(c_size_t), value, intent(in) :: namelen
       character(len=1,kind=c_char), dimension(namelen),  intent(inout) ::  name
     end function gethostname
  end interface

contains

  subroutine pinning_info(host,numthreads,cores)

    IMPLICIT NONE

    character(*), intent(out) :: host
    character(*), intent(out) :: cores
    integer, intent(out) :: numthreads

    integer, allocatable :: procids(:)
    integer threadnum,ic
!$  integer omp_get_max_threads,omp_get_thread_num
    character(3) core
    numthreads=1 !fallback
    threadnum=0 !fallback

    cores=""
    
!$ numthreads=omp_get_max_threads()
    call hostname(host)
    allocate(procids(0:numthreads-1))
    procids(:)=-1
!$OMP PARALLEL DO ORDERED SCHEDULE(static,1) &
!$OMP   PRIVATE(ic,threadnum) SHARED(procids)
    do ic=1,numthreads
!$     threadnum=omp_get_thread_num()
       procids(threadnum)=threadGetProcessorId()
    enddo
    do ic=0,numthreads-1
        write(core,'(I3)') procids(ic)
       cores = trim(cores)//' '//trim(core) 
    enddo
    cores=adjustl(trim(cores))
    
    deallocate(procids)

  end subroutine pinning_info

  pure function c_to_f_string(c_string) result(f_string)
    use, intrinsic :: iso_c_binding, only: c_char, c_null_char
    character(kind=c_char,len=1), intent(in) :: c_string(:)
    character(len=:), allocatable :: f_string
    integer i, n
    i = 1
    do
       if (c_string(i) == c_null_char) exit
       i = i + 1
    end do
    n = i - 1  ! exclude c_null_char
    allocate(character(len=n) :: f_string)
    f_string = transfer(c_string(1:n), f_string)
  end function c_to_f_string

  subroutine hostname(name)

    use, intrinsic  :: iso_c_binding, only: c_char, c_int, c_size_t
    character(*), intent(out) :: name
    integer(c_int) :: status
    integer,parameter :: HOST_NAME_MAX=255
    integer(c_size_t) :: lenstr
    character(kind=c_char,len=1),dimension(HOST_NAME_MAX) :: cstr_hostname
    lenstr=HOST_NAME_MAX
    status = gethostname(cstr_hostname,lenstr)
    name = c_to_f_string(cstr_hostname)

  end subroutine hostname

end module mod_getcpu
