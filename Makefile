COMPILER ?= GCC

ifeq ($(COMPILER),INTEL)
CC=icx
FC=ifx
LD=ifx
FLAGS=-qopenmp
endif

ifeq ($(COMPILER),GCC)
CC=gcc
FC=gfortran
LD=gfortran
FLAGS=-fopenmp
endif

MPIFC=mpi$(FC)
MPILD=mpi$(LD)


MPIOBJS=getcpu.o mod_getcpu.o pinning_mpi.o
OMPOBJS=getcpu.o mod_getcpu.o pinning_omp.o

default: pinning_mpi.x

pinning_omp.x: $(OMPOBJS)
	$(LD) $(FLAGS) -o $@ $^

pinning_mpi.x: $(MPIOBJS)
	$(MPILD) $(FLAGS) -o $@ $^



pinning_omp.o: pinning_omp.f90
	$(FC) $(FLAGS) -c $<

pinning_mpi.o: pinning_mpi.f90
	$(MPIFC) $(FLAGS) -c $<

%.o: %.f90
	$(FC) $(FLAGS) -c $<
%.o: %.c
	$(CC) $(FLAGS) -c $<

clean:
	rm -f *.o *.mod

tar:
	tar cvzf fpincheck.tgz Makefile getcpu.c mod_getcpu.f90 pinning_mpi.f90 pinning_omp.f90
