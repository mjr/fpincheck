program test

  use mod_getcpu
  implicit none

  integer numthreads
  character(2048) :: cores
  character(128) :: host

  call pinning_info(host,numthreads,cores)
  
  write(*,'(A9,I4,A6,A,A8,A)') &
         &'THREADS:',numthreads,&
         & ' HOST: ',trim(adjustl(host)),&
         &' CORES: ',trim(adjustl(cores))

end program test
