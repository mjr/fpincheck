program test

  use mpi
  use mod_getcpu
  implicit none

  integer myrank,numprocs,numthreads,ierr
  character(2048) :: cores
  character(128) :: host
  
  call MPI_Init(ierr)

  call MPI_Comm_rank(MPI_COMM_WORLD, myrank, ierr)
  call MPI_Comm_size(MPI_COMM_WORLD, numprocs, ierr)

  call pinning_info(host,numthreads,cores)
  
  write(*,'(A9,I4,A9,I4,A6,I4,A8,A,A8,A)') 'TASKS:',numprocs,&
         &'THREADS:',numthreads,&
         &'RANK:',myrank,'HOST: ',trim(adjustl(host)),&
         &' CORES: ',trim(adjustl(cores))

  
  call MPI_Finalize(ierr)

end program test
